
#include <iostream>
#include <vector>

using namespace std;

void criba_eratostenes(int N) {
    vector<bool> es_primo(N + 1, true); 

    for (int p = 2; p * p <= N; p++) {
       
        if (es_primo[p]) {
            for (int i = p * p; i <= N; i += p)
                es_primo[i] = false;
        }
    }


    cout << "N�meros primos hasta " << N << ":\n";
    for (int i = 2; i <= N; i++) {
        if (es_primo[i])
            cout << i << " ";
    }
    cout << endl;
}

int main() {
    int N;
    cout << "Ingrese el valor de N: ";
    cin >> N;

    criba_eratostenes(N);

    system("pause");
    return 0;
}
